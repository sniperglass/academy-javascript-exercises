//REVERSE

function reverse(string) {
    
        return string.split("").reverse().join("");
}    



reverse("Propulsion Academy") // "ymedacA noisluporP"
reverse("Hello") // "olleH"
reverse("abcd") // "dcba"




//FACTORIAL

function factorial(n) {
    if (n === 0 || n === 1) {
      return 1;
    }
    for (let i = n - 1; i >= 1; i--){
  
      n = n*i;
    }
    return n;
  }
  
  factorial(5);




  //LONGEST WORD 


  function longest_word(sentence) {
      let sentenceSplit = sentence.split(' ');
      let result = 0;

      for(let i = 0; i < sentenceSplit.length; i++) {
          if (sentenceSplit[i] > result) {
              result = sentenceSplit[i];
          }
          return result;
      }

}

longest_word("This is an amazing test") // "amazing"
longest_word("Laurent Colin") // "Laurent"
longest_word("Propulsion 123") // "Propulsion"




//SUM NUMS

function sum_nums(num) {

    let result = 0;
    for (let i = 1; i <= num; i++) {
        result += i;
    }

    return result;
}

sum_nums(6) // 21
sum_nums(1) // 1
sum_nums(0) // 0



//TIME CONVERSION

function time_conversion(minutes) {
    let min = minutes;
    let rawHours = (min/60);
    let finalHours = Math.floor(rawHours);
    let rawMinutes = (finalHours - rawHours) * 60;
    let finalMinutes = Math.round(rawMinutes);

    return  finalHours + ':' + finalMinutes;
}

time_conversion(200);

time_conversion(155) // "02:35"
time_conversion(61) // "01:01"



//COUNT VOWELS

function count_vowels(string) {

    let newString = string.toString();
    let result = 0;

    for(let i = 0; i <= string.length; i++) {
      if (string.charAt[i] === 'a' || string.charAt[i] === 'e' 
      || string.charAt[i] === 'i' || string.charAt[i] === 'o' || string.charAt[i] === 'u'
      || string.charAt[i] === 'y') {
          result += 1;
      }
      count_vowels('alphabet');
    }

}

count_vowels("alphabet") // 3
count_vowels("Propulsion Academy") // 7



// PALINDROME

function palindrome(string) {

    let strlength = string.length;

    for (let i = 0; i < strlength; i++) {
        if (string[i] !== string[strlength-1-i]) {
            return false;
        }
    }

    return true;

}

palindrome("ABBA") // true
palindrome("AbbA") // true
palindrome("abcd") // false


//MOST LETTERS

function nearby_az(string) {

    let strlength = string.length;

    for (let i = 0; i < strlength; i++) {
        if (string.charAt[2] !== 'z') {
            return false;
        } else {
            return true;
        }
    }
    
}

nearby_az("abcz") // true
nearby_az("abba") // false



//TWO SUMS 

function two_sum(nums) {

    let newNums = [];

    for (let i = 0; i < nums.length; i++) {

        for (let m = i + 1; m < nums.length; m++) {
             if (nums[i] + nums[j] == 0) {
                newNums.push([arr[i], arr[j]]);
          } else {
              return null;
          }
        }
    }
    return newNums;

}

two_sum([1, 3, -1, 5, -3]) // [[0, 2], [1, 4]]
two_sum([1, 5, 3, -4]) // null





//IS POWER OF TWO

function is_power_of_two(num) {
    
    if (Math.log2(num) % 1 === 0) {
        return true; {
        } else {
          return false
        }
    }

}

is_power_of_two(8) // true
is_power_of_two(24) // false






//REPEAT A STRING 

function repeat_string_num_times(str, num) {

    if (num < 0) {
        return '';
    } else {
        return str.repeat(num);
    }

}

repeat_string_num_times("abc", 3) // 'abcabcabc'
repeat_string_num_times("abc", 1) // 'abc'
repeat_string_num_times("abc", 0) // ''
repeat_string_num_times("abc", -1) // ''


//SUM ALL NUMBERS IN A RANGE

function add_all(arr) {

    let finalArr = [];
    let finalSumOfNumbers = 0;
    const number = (accumulator, currentValue) => 
    accumulator + currentValue;

    arr.sort(function(a,b){return a-b});

    for(let i = arr[0]; i <= arr[1]; i++) {
        finalArr.push(i);
    }

    finalSumOfNumbers = finalArr.reduce(number);

    return finalSumOfNumbers;
    
}

add_all([1, 4]);


//TRUE OR FALSE

function is_it_true(args) {

    if(args === true || args === false) {
        return true
    } else {
        return false
    }
}

is_it_true(true) // true
is_it_true(false) // true




//IS IT AN ANAGRAM


function largest_of_four(string1, string2) {

    return correctString(string1) === correctString(string2)

}

function correctString(arr) {
    return arr.split('').sort().join('');
}